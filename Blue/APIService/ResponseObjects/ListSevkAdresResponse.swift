//
//  ListSevkAdresResponse.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct ListSevkAdresResponse: Mappable {
    let list: [SevkAdres]

    init(map: Mapper) throws {
        list = try map.from("List")
    }
}

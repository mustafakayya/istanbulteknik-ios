//
//  ListBlueParaResponse.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct ListBlueParaResponse: Mappable {
    let list: [BluePara]
    let toplamBluePara: Double

    init(map: Mapper) throws {
        list = try map.from("List")
        toplamBluePara = try map.from("ToplamBluePara")
    }
}

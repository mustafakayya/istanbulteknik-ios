//
//  SaveBasvuruResponse.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct SaveBasvuruResponse: Mappable {

    let basvuru: Basvuru

    init(map: Mapper) throws {
        basvuru = try map.from("Basvuru")
    }
}

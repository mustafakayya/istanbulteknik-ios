//
//  ListSiparisResponse.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct ListSiparisResponse: Mappable {

    let list: [Siparis]

    init(map: Mapper) throws {
        list = try map.from("List")
    }
}

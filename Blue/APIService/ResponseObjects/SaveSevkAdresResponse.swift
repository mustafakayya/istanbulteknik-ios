//
//  SaveSevkAdresResponse.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct SaveSevkAdresResponse: Mappable {
    let adres: SevkAdres

    init(map: Mapper) throws {
        adres = try map.from("SevkAdres")
    }
}

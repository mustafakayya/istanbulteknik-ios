//
//  GetSevkAdresResponse.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct GetSevkAdresResponse: Mappable {

    let data: SevkAdres
    let listSehir: [Sehir]
    let listIlce: [Ilce]

    init(map: Mapper) throws {
        data = try map.from("Data")
        listSehir = map.optionalFrom("ListSehir") ?? []
        listIlce = map.optionalFrom("ListIlce") ?? []
    }
}

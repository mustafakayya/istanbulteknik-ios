//
//  EmptyResponse.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct EmptyResponse: Mappable {
    init(map: Mapper) throws {
    }
}

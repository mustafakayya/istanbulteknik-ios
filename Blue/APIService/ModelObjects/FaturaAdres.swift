//
//  FaturaAdres.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct FaturaAdres {
    let adres: String
    let vergiDairesi: String
    let vergiNumarasi: String

    var json: [String: Any] {
        return [
            "Adres": adres,
            "VergiDairesi": vergiDairesi,
            "VergiNumarasi": vergiNumarasi
        ]
    }
}

extension FaturaAdres: Mappable {
    init(map: Mapper) throws {
        adres = try map.from("Adres")
        vergiDairesi = try map.from("VergiDairesi")
        vergiNumarasi = try map.from("VergiNumarasi")
    }
}

//
//  BluePara.swift
//  Blue
//
//  Created by Mustafa Yusuf on 24.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct BluePara: Mappable {
    let id: Int
    let aciklama: String
    let paraMiktari: Double
    let islemTipi: Int
    let islem: String
    let ekleyen: String
    let eklemeTarihi: Date
    let bazTarihi: Date

    init(map: Mapper) throws {
        id = try map.from("BlueParaId")
        aciklama = try map.from("Aciklama")
        paraMiktari = try map.from("ParaMiktari")
        islemTipi = try map.from("IslemTipi")
        islem = try map.from("Islem")
        ekleyen = try map.from("Ekleyen")
        eklemeTarihi = try map.from("EklenmeTarihi", transformation: parseDate(from:))
        bazTarihi = try map.from("BazTarih", transformation: parseDate(from:))
    }
}

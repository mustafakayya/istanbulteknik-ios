//
//  Ilce.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct Ilce: Mappable {
    let id: Int
    let ad: String
    let sehirId: Int

    init(map: Mapper) throws {
        id = try map.from("Id")
        ad = try map.from("Ad")
        sehirId = try map.from("SehirId")
    }
}

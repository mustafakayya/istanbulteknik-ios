//
//  Basvuru.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct Basvuru {
    let id: Int
    let basvuruNumarasi: String
    let unvan: String
    let vergiDairesi: String
    let vergiNumarasi: String
    let tcKimlikNo: String
    let yetkili: String
    let yetkiliUnvan: String
    let eposta: String
    let telefonMobil: String
    let telefonSabit: String
    let adres: String
    let basvuruNotu: String
    let sozlesmeOnayi: Bool
}

extension Basvuru {
    var json: [String: Any] {
        return [
            "BasvuruId": id,
            "BasvuruNumarasi": basvuruNumarasi,
            "Unvan": unvan,
            "VergiDairesi": vergiDairesi,
            "VergiNumarasi": vergiNumarasi,
            "TcKimlikNo": tcKimlikNo,
            "Yetkili": yetkili,
            "YetkiliUnvan": yetkiliUnvan,
            "EPosta": eposta,
            "TelefonMobil": telefonMobil,
            "TelefonSabit": telefonSabit,
            "Adres": adres,
            "BasvuruNotu": basvuruNotu,
            "SozlesmeOnayi": sozlesmeOnayi
        ]
    }
}

extension Basvuru: Mappable {
    init(map: Mapper) throws {
        id = try map.from("BasvuruId")
        basvuruNumarasi = try map.from("BasvuruNumarasi")
        unvan = try map.from("Unvan")
        vergiDairesi = try map.from("VergiDairesi")
        vergiNumarasi = try map.from("VergiNumarasi")
        tcKimlikNo = try map.from("TcKimlikNo")
        yetkili = try map.from("Yetkili")
        yetkiliUnvan = try map.from("YetkiliUnvan")
        eposta = try map.from("EPosta")
        telefonMobil = try map.from("TelefonMobil")
        telefonSabit = try map.from("TelefonSabit")
        adres = try map.from("Adres")
        basvuruNotu = try map.from("BasvuruNotu")
        sozlesmeOnayi = try map.from("SozlesmeOnayi")
    }
}

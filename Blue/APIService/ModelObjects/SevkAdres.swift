//
//  SevkAdres.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct SevkAdres {
    let id: Int
    let baslik: String
    let alici: String
    let telefon: String
    let adres: String
    let postaKodu: Int
    let sehirId: Int
    let ilceId: Int
    let sehir: String
    let ilce: String
    let vergiDairesi: String
    let vergiNumarasi: String
}

extension SevkAdres: Mappable {
    init(map: Mapper) throws {
        id = try map.from("SevkAdresiId")
        baslik = try map.from("Baslik")
        alici = try map.from("Alici")
        telefon = try map.from("Telefon")
        adres = try map.from("Adres")
        postaKodu = try map.from("PostaKodu")
        sehirId = try map.from("SehirId")
        ilceId = try map.from("IlceId")
        sehir = try map.from("Sehir")
        ilce = try map.from("Ilce")
        vergiDairesi = try map.from("VergiDairesi")
        vergiNumarasi = try map.from("VergiNumarasi")
    }
}

extension SevkAdres {
    var json: [String: Any] {
        return [
            "SevkAdresiId": id,
            "Baslik": baslik,
            "Alici": alici,
            "Telefon": telefon,
            "Adres": adres,
            "PostaKodu": postaKodu,
            "SehirId": sehirId,
            "IlceId": ilceId,
            "Sehir": sehir,
            "Ilce": ilce,
            "VergiDairesi": vergiDairesi,
            "VergiNumarasi": vergiNumarasi
        ]
    }
}

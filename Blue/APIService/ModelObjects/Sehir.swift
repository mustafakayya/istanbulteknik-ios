//
//  Sehir.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct Sehir: Mappable {
    let id: Int
    let ad: String
    let ulkeId: Int

    init(map: Mapper) throws {
        id = try map.from("Id")
        ad = try map.from("Ad")
        ulkeId = try map.from("UlkeId")
    }
}

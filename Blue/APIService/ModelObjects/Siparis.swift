//
//  Siparis.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

struct Siparis {
    let id: Int
    let ozelSiparisNo: String
    let siparisSahibi: String
    let odemeSekli: String
    let gerceklesenTutar: Double
    let siparisTarihi: Date
    let iptalEdilebilirMi: Bool
}

extension Siparis: Mappable {
    init(map: Mapper) throws {
        id = try map.from("SiparisId")
        ozelSiparisNo = try map.from("OzelSiparisNo")
        siparisSahibi = try map.from("SiparisSahibi")
        odemeSekli = try map.from("OdemeSekli")
        gerceklesenTutar = try map.from("GerceklesenTutar")
        siparisTarihi = try map.from("SiparisTarihi", transformation: parseDate(from:))
        iptalEdilebilirMi = try map.from("IptalEdilebilirMi")
    }
}

extension Siparis {
    var json: [String: Any] {
        return [
            "SiparisId": id,
            "OzelSiparisNo": ozelSiparisNo,
            "SiparisSahibi": siparisSahibi,
            "OdemeSekli": odemeSekli,
            "GerceklesenTutar": gerceklesenTutar,
            "SiparisTarihi": convert(date: siparisTarihi),
            "IptalEdilebilirMi": iptalEdilebilirMi
        ]
    }
}

//
//  Login.swift
//  Blue
//
//  Created by Mustafa Yusuf on 23.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

extension NetworkService {
    func login(username: String, password: String, completion: @escaping RequestCompletion<TokenResponse>) {
        let params: [String: Any] = [
            "Username": username,
            "Password": password
        ]
        send(path: "Hesap/Login", parameters: params, completion: completion)
    }

    func listBluePara(completion: @escaping RequestCompletion<ListBlueParaResponse>) {
        send(path: "Hesap/ListBluePara", completion: completion)
    }

    func listSevkAdress(completion: @escaping RequestCompletion<ListSevkAdresResponse>) {
        send(path: "Hesap/ListSevkAdres", completion: completion)
    }

    func getSevkAdres(id: Int, completion: @escaping RequestCompletion<GetSevkAdresResponse>) {
        send(path: "Hesap/GetSevkAdres", parameters: ["SevkAdresId": id], completion: completion)
    }

    func saveSevkAdres(_ adres: SevkAdres, completion: @escaping RequestCompletion<SaveSevkAdresResponse>) {
        let params = ["SevkAdres": adres.json]
        send(path: "Hesap/SaveSevkAdres", parameters: params, completion: completion)
    }

    func saveFaturaAdres(_ adres: FaturaAdres, completion: @escaping RequestCompletion<EmptyResponse>) {
        let params = ["FaturaAdres": adres.json]
        send(path: "Hesap/SaveFaturaAdres", parameters: params, completion: completion)
    }

    func saveBasvuru(_ basvuru: Basvuru, completion: @escaping RequestCompletion<SaveBasvuruResponse>) {
        let params = ["Basvuru": basvuru.json]
        send(path: "Hesap/SaveBasvuru", parameters: params, completion: completion)
    }

    func listSiparis(completion: @escaping RequestCompletion<ListSiparisResponse>) {
        send(path: "Hesap/ListSiparis", completion: completion)
    }
}

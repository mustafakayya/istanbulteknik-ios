//
//  NetworkService.swift
//  Blue
//
//  Created by Mustafa Yusuf on 22.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Alamofire
import Mapper

let baseUrl = "http://apiistek.argede.com.tr/api"

typealias RequestCompletion<T: Mappable> = (Result<Response<T>>) -> Void

final class NetworkService {

    private var token: String?

    func send<T>(path: String, parameters: [String: Any] = [:], completion: @escaping RequestCompletion<T>) {
        let url = baseUrl + "/\(path)"

        var params = parameters
        if let token = token {
            params["Token"] = token
        }

        let request = Alamofire.request(url, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding(), headers: ["content-type": "application/json"])

        request.validate().responseJSON { response in
            switch response.result {
            case .failure(let error):
                completion(.error(error))
            case .success(let json):
                do {
                    let mapper = Mapper(JSON: json as! NSDictionary)
                    let apiResponse: Response<T> = try mapper.from("")
                    completion(.success(apiResponse))
                } catch {
                    completion(.error(error))
                }
            }
        }
    }

    func getToken(completion: @escaping RequestCompletion<TokenResponse>) {
        send(path: "Hesap/GetToken") { (result: Result<Response<TokenResponse>>) -> Void in
            switch result {
            case .success(let response):
                switch response {
                case .success(let value, _, _):
                    self.token = value.token
                default: break
                }
            default: break
            }

            completion(result)
        }
    }
    
    func forgotPassword(email : String , completion: @escaping RequestCompletion<EmptyResponse>) {
        let params = ["Email" : email]
        send(path: "Hesap/SifremiUnuttum", parameters : params) { (result: Result<Response<EmptyResponse>>) -> Void in
            completion(result)
        }
    }
}

//
//  Response.swift
//  Blue
//
//  Created by Mustafa Yusuf on 23.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

enum Result<T> {
    case success(T)
    case error(Error)
}

enum ResponseStatus: String {
    case success = "00"
    case information = "01"
    case warning = "02"
    case error = "03"
    case requireLogin = "10"
    case tokenExpired = "11"
    case unknown
}

struct ListInputError: Mappable {
    let inputName: String
    let error: String

    init(map: Mapper) throws {
        inputName = try map.from("InputName")
        error = try map.from("Error")
    }
}

protocol ResponseType {
    associatedtype ResponseObject

    var isSuccess: Bool { get }
}

enum Response<T: Mappable>: Mappable, ResponseType {
    typealias ResponseObject = T

    case success(result: T, status: ResponseStatus, message: String)
    case error(message: String?, inputErrors: [ListInputError])

    init(map: Mapper) throws {
        let status = ResponseStatus.init(rawValue: try map.from("ResponseCode")) ?? .unknown
        let message: String =  map.optionalFrom("ResponseMessage") ?? ""

        switch status {
        case .error:
            self = .error(message: message, inputErrors: map.optionalFrom("ListInputError") ?? [])
        case .unknown:
            self = .error(message: nil, inputErrors: [])
        default:
            self = .success(result: try map.from(""), status: status, message: message)
        }
    }

    var isSuccess: Bool {
        switch self {
        case .success: return true
        case .error: return false
        }
    }
}

extension Result where T: ResponseType {
    var isSuccess: Bool {
        switch self {
        case .error: return false
        case .success(let response): return response.isSuccess
        }
    }
}

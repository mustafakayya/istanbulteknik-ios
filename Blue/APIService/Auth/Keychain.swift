//
//  Keychain.swift
//  FindFriends
//
//  Created by Serhan Aksüt on 6.02.2018.
//  Copyright © 2018 Serhan Aksüt. All rights reserved.
//

import KeychainSwift

struct Credential {
    struct Key {
        let keyName: String
        
        static var token: Key {
            return Key(keyName: "auth_token")
        }
    }
    
    let key: Key
    let value: String

    static func token(_ value: String) -> Credential {
        return Credential(key: .token, value: value)
    }
}

final class Keychain {
    private static let keychain = KeychainSwift()
    
    static func save(credentials: [Credential]) {
        credentials.forEach { keychain.set($0.value, forKey: $0.key.keyName) }
    }
    
    static func delete(_ keys: [Credential.Key]) {
        keys.forEach { keychain.delete($0.keyName) }
    }
    
    static func get(_ key: Credential.Key) -> Credential? {
        return keychain.get(key.keyName).map { Credential(key: key, value: $0) }
    }
    
    static func has(_ key: Credential.Key) -> Bool {
        return keychain.get(key.keyName) != nil
    }
    
    static func hasUser(with keys: Credential.Key...) -> Bool {
        return !(keys.contains(where: { !Keychain.has($0) }))
    }
}

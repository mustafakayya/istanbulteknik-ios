//
//  AppDelegate.swift
//  Blue
//
//  Created by Mustafa Yusuf on 22.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let appStarter = AppStarter(window: UIWindow(frame: UIScreen.main.bounds))


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        self.window = appStarter.window
        appStarter.startApp()

        return true
    }
}

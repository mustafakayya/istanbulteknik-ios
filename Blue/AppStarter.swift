//
//  AppStarter.swift
//  Blue
//
//  Created by Mustafa Yusuf on 22.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

final class AppStarter {

    let window: UIWindow
    let service = NetworkService()

    init(window: UIWindow) {
        self.window = window
    }

    func startApp() {
        let viewController = LoginViewController(service: service)
        window.rootViewController = viewController
        window.makeKeyAndVisible()

//        viewController.activityIndicator.startAnimating()
//        checkLoginStatus()
    }
}

extension AppStarter {
    func checkLoginStatus() {
        if Keychain.has(.token) {

        } else {
            getGeneralToken()
        }
    }

    func getGeneralToken() {
        service.getToken { [unowned self] result in
            if result.isSuccess {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self.goToHomePage()
                })
            }
        }
    }
}

extension AppStarter {
    func goToHomePage() {
        let login = LoginViewController(service: service)
        window.rootViewController = login
    }
}

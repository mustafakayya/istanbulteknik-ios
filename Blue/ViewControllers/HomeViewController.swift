//
//  HomeViewController.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

final class HomeViewController: UIViewController {
    private let service: NetworkService

    init(service: NetworkService) {
        self.service = service
        super.init(nibName: "HomeViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

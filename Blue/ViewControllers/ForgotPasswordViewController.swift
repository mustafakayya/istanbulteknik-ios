//
//  ForgotPasswordViewController.swift
//  Blue
//
//  Created by Mustafa KAYA on 3/28/18.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    let service : NetworkService
    
    init(service: NetworkService) {
        self.service = service
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.fixTextfieldPlaceholder()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCloseClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSendClicked(_ sender: Any) {
        guard let email = emailTextField.text , email.count > 0 else {
            showAlertDialog(message: "Email adresi bos olamaz")
            return
        }
        service.forgotPassword(email: email) { [weak self] result in
            
            switch result {
            case .success(let response):
                switch response {
                case .success(_, let status, let message):
                    if(status == ResponseStatus.success){
                        self?.showAlertDialog(title : "",message: message){ value in
                            self?.dismiss(animated: true, completion: nil)
                        }
                    }else{
                        self?.showAlertDialog(message: message)
                    }
                case .error(let message ,_):
                    self?.showAlertDialog(message: message!)
                }
            default: break
            }
        }
    }
}

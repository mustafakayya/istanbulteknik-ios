//
//  SplashViewController.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

final class SplashViewController: UIViewController {

    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!

    let service = NetworkService()
    var splasViewControllerDone: ((NetworkService) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.type = .ballBeat
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//
//  LoginViewController.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController, KeyboardHandler, UITextFieldDelegate {
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    @IBAction func backgroundTap(_ sender: Any) {
        self.view.endEditing(true)
    }

    var keyboardToken: NSObjectProtocol?
    let service: NetworkService

    private var username: String?
    private var password: String?

    init(service: NetworkService) {
        self.service = service
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        listenKeyboardEvents()
        [passwordTextfield, usernameTextfield].forEach { $0.fixTextfieldPlaceholder() }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: Actions

    @IBAction func onUsernameTextFieldChanged(_ sender: Any) {
        username = usernameTextfield.text
    }

    @IBAction func onPasswordTextFieldChanged(_ sender: Any) {
        password = passwordTextfield.text
    }

    @IBAction func onLoginClicked(_ sender: Any) {
        login()
    }

    @IBAction func onForgotPasswordClicked(_ sender: Any) {
        let forgotVC = ForgotPasswordViewController(service: service)
        show(forgotVC, sender: nil)
    }

    // MARK: KeyboardHandler

    func keyboardChange(frame: CGRect, duration: TimeInterval, curve: UIViewAnimationOptions) {
        let height = view.bounds.height
        if frame.origin.y < height {
            // show
            topConstraint.constant = 40
            widthConstraint.constant = 50
            bottomConstraint.constant = frame.size.height
        } else {
            // hide
            topConstraint.constant = 108
            widthConstraint.constant = 103
            bottomConstraint.constant = 0
        }

        UIView.animate(withDuration: duration, delay: 0, options: curve, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    // MARK: UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextfield:
            passwordTextfield.becomeFirstResponder()
        case passwordTextfield:
            passwordTextfield.resignFirstResponder()
            login()
        default: break
        }

        return true
    }

    // MARK: Network Requests

    private func login() {
        guard isFormValid() else { return }

        view.endEditing(true)
        ActivityIndicator.show(message: "Giriş Yapılıyor...")
        service.login(username: username!, password: password!) { result in
            ActivityIndicator.hide()
            print("Login sended: result is => \(result)")
        }
    }

    private func isFormValid() -> Bool {
        guard
            FormValidation.validate(field: usernameTextfield, text: username, validation: FormValidation.username(_:)),
            FormValidation.validate(field: passwordTextfield, text: password, validation: FormValidation.password(_:))
            else {
                return false
        }

        return true
    }
}



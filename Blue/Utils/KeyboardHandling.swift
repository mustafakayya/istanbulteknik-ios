//
//  KeyboardHandling.swift
//  Blue
//
//  Created by Mustafa Yusuf on 2.04.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

protocol KeyboardHandler: class {
    var keyboardToken: NSObjectProtocol? { get set }

    func keyboardChange(frame: CGRect, duration: TimeInterval, curve: UIViewAnimationOptions)
}

extension KeyboardHandler {
    func listenKeyboardEvents() {
        keyboardToken = NotificationCenter.default.addObserver(forName: .UIKeyboardWillChangeFrame, object: nil, queue: nil) { [weak self] notification in
            guard
                let _self = self,
                let options = notification.userInfo as? [String: Any],
                let frame = (options[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
                let duration = options[UIKeyboardAnimationDurationUserInfoKey] as? Double,
                let curveRaw = options[UIKeyboardAnimationCurveUserInfoKey] as? UInt
                else { return }

             let curve = UIViewAnimationOptions(rawValue: curveRaw)

            _self.keyboardChange(frame: frame, duration: duration, curve: curve)
        }
    }
}

//
//  ActivityIndicator.swift
//  Blue
//
//  Created by Mustafa Yusuf on 3.04.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import NVActivityIndicatorView

enum ActivityIndicator {
    static func show(message: String? = nil) {
        let activityData = ActivityData(message: message, messageFont: nil, type: .ballBeat, color: nil, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: 1, backgroundColor: nil, textColor: nil)

        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }

    static func hide() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }

    static func changeMessage(_ message: String) {
        NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
    }
}

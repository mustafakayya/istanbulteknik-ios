//
//  KeyboardToolbar.swift
//  Blue
//
//  Created by Mustafa Yusuf on 3.04.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

final class KeyboardToolbar: UIToolbar {

    private var fields: [Weak<UITextField>] = []
    weak var activeTextField: UITextField?

    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setFields(_ fields: [UITextField]) {
        self.fields = fields.map(Weak.init)
        makeButtons()
        setInputSources()
    }

    private func makeButtons() {
        let preButton = button(title: "Önceki", action: #selector(preAction))
        let nextBuuton = button(title: "Sonraki", action: #selector(nextAction))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = button(title: "Bitti", action: #selector(doneAction))

        self.items = [preButton, nextBuuton, space, doneButton]
    }

    private func setInputSources() {
        fields.forEach { $0.value?.inputAccessoryView = self }
    }

    private func button(title: String, action: Selector) -> UIBarButtonItem {
        let barButton = UIBarButtonItem(title: title, style: .plain, target: self, action: action)
        return barButton
    }

    // MARK: Actions

    @objc func preAction() {
        guard let active = activeTextField.map(Weak.init),
        let index = fields.index(of: active) else { return }
        let preIndex = fields.index(before: index)

        if preIndex < fields.startIndex {
            doneAction()
        } else {
            activeTextField = fields[preIndex].value
            activeTextField?.becomeFirstResponder()
        }
    }

    @objc func nextAction() {
        guard let active = activeTextField.map(Weak.init),
        let index = fields.index(of: active) else { return }
        let nextIndex = fields.index(after: index)

        if nextIndex >= fields.endIndex {
            doneAction()
        } else {
            activeTextField = fields[nextIndex].value
            activeTextField?.becomeFirstResponder()
        }
    }

    @objc func doneAction() {
        activeTextField?.resignFirstResponder()
        activeTextField = nil
    }
}

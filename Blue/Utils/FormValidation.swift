//
//  FormValidation.swift
//  Blue
//
//  Created by Mustafa Yusuf on 3.04.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import UIKit

enum FormValidation {
    static func validate(field: UITextField?, text: String?, validation: (String?) -> (Bool, String?)) -> Bool {
        let (isValid, message) = validation(text)
        if !isValid {
            field?.shake()
            if let message = message {
                DropDownMessage.show(message: message)
            }
        }
        return isValid
    }

    static func username(_ text: String?) -> (Bool, String?) {
        var valid = true
        var message: String? = nil

        switch text {
        case nil:
            valid = false
            message = "Kullanıcı adı boş olamaz"
        default: break
        }

        return (valid, message)
    }

    static func password(_ text: String?) -> (Bool, String?) {
        var valid = true
        var message: String? = nil

        switch text {
        case nil:
            valid = false
            message = "Kullanıcı adı boş olamaz"
        default: break
        }

        return (valid, message)
    }
}


//
//  DropDownMessages.swift
//  Blue
//
//  Created by Mustafa Yusuf on 3.04.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import SwiftMessages

enum DropDownMessage {
    static func show(message: String, title: String? = nil, style: Theme = .error) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(style)
        view.configureContent(title: title ?? "", body: message)

        SwiftMessages.show(view: view)
    }
}

//
//  WeakBox.swift
//  Blue
//
//  Created by Mustafa Yusuf on 3.04.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation

struct Weak<T: AnyObject> {
    weak var value: T?
}

extension Weak: Equatable where T: Equatable {
    static func ==(lhs: Weak<T>, rhs: Weak<T>) -> Bool {
        return lhs.value == rhs.value
    }
}

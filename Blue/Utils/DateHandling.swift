//
//  DateHandling.swift
//  Blue
//
//  Created by Mustafa Yusuf on 25.03.2018.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import Mapper

private var jsonDateFormatter: DateFormatter = {
    let f = DateFormatter()
    f.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZZZ"
    return f
}()

func parseDate(from string: Any) throws -> Date {
    guard
        let dateStr = string as? String,
        let date = jsonDateFormatter.date(from: dateStr)
    else {
        throw MapperError.convertibleError(value: string, type: String.self)
    }

    // 2018-03-25T01:17:22.4213402+03:00
    return date
}

func convert(date: Date) -> String {
    return jsonDateFormatter.string(from: date)
    // 2018-03-25T01:17:22.4213402+03:00
}

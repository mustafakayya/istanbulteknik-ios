//
//  Extensions.swift
//  Blue
//
//  Created by Mustafa KAYA on 3/28/18.
//  Copyright © 2018 OrganizationX. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    func showAlertDialog(title : String = "Hata" , message : String
        , completion: @escaping (UIAlertAction) -> () = { value in }) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Kapat", style: UIAlertActionStyle.default,handler:completion))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension UITextField {
    func fixTextfieldPlaceholder(_color : UIColor = UIColor.white) {
        let attributes: [NSAttributedStringKey: Any] = [.foregroundColor: _color]
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: attributes)
    }
}
